public class AddNumber{

	public static void main(String args[]){
		int x = 27;
		int y = 10;
		int result = 0;

		result = x + y;
		System.out.println("Addition of       :"+ x +"+"+ y + " =" + result);

		result = x - y;
		System.out.println("Substration of    :"+ x +"-"+ y + " =" + result);

		result = x * y;
		System.out.println("Multiplication of  :"+ x +"*"+ y + " =" + result);

		result = x / y;
		System.out.println("Division of         :"+ x +"/"+ y + " =" + result);
	}
}